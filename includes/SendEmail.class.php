<?php

class SendEmail { 

	//alterar depois para o domínio correto
	private $from="imanust@imanust.com";
	private $email;
	
	function __construct($email) {
		$this->email = $email;
	}
	
	function verify_email(){
	
		if (!preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/',$this->email)){
			return false;
		}
		else{
			return true;
		}
	  
	
	}
	
	function send_email($pass) {
		//não está enviando email por cause do remetente
		$subject="Cadastro Imanust";
		$msg="Seu password para acesso ao sistema Imanust é ".$pass.". Mantenha-o seguro e delete esse e-mail assim que possível.";
		mail($this->email, $subject, $msg, "-r".$this->from);
	
	}
	
	function send_email_to_admin($userEmail, $idPermission) {
		$subject="Solicitação de permissão";
		$msg="O usuário de email ".$userEmail." solicitou que seja dado a ele a permissão de ID ".$idPermission.".";
		mail($this->email, $subject, $msg, "-r".$this->from);
	}

	function send_email_to_user($status, $comment) {
		//não está enviando email por cause do remetente
		$subject="Resposta Solicitação de Permissão";
		$msg="A sua solitação de permissão foi ".$status.".<br>Comentário do admin: ".$comment;
		mail($this->email, $subject, $msg, "-r".$this->from);
	}
}

?>