<?php

class URLDispacherResult
{
	public $include;
	public $found;
	public $data;
	
	public function __construct($include,$found=TRUE,$data=[])
	{
		$this->include = $include;
		$this->found = $found;
		$this->data = $data;
	}
}

class URLPatterns
{
	private $patterns;
	
	public function __construct($patterns = [])
	{
		$this->patterns = $patterns;
	}
	
	public function match($url)
	{
		foreach($this->patterns as $pattern => $value)
		{
			if(preg_match($pattern,$url,$matches))
			{
				return new URLDispacherResult($value,TRUE,$matches);
			}
		}
		
		return NULL;
	}
}

class URLDispacher
{
	private $patterns;
	private $base;
	private $not_found;
	
	public function __construct($patterns,$base="",$not_found="")
	{
		$this->patterns = $patterns;
		$this->base = $base;
		$this->not_found = $not_found;
	}
	
	public function match($baseurl,$first_try=TRUE)
	{
		if(!preg_match($this->base,$baseurl))
		{
			return new URLDispacherResult($this->not_found,FALSE);
		}
		
		$url = preg_replace($this->base, "", $baseurl);
		
		$ret = $this->patterns->match($url);
		
		if($ret)
		{
			return $ret;
		}
		else if($first_try)
		{
			if(substr($baseurl,-1)=="/")
			{
				return $this->match(substr($baseurl,0,-1),FALSE);
			}
			else
			{
				return $this->match($baseurl."/",FALSE);
			}
		}
		else
		{
			return new URLDispacherResult($this->not_found,FALSE);
		}
	}
}

?>
