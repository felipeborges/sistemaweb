<?php

class QueryBuilder {
    private $tableData;
    
    function __construct($table=NULL) {
        $this->tableData = $table;
    }
    
    public function insertTable($id_station) {
            return sprintf("INSERT INTO Tabela (estacao) VALUES (%d);", $id_station);
    }
    
    public function insertStation($criador, $id=NULL, $cidade="", $tipo=0)
    {
        $header = $this->tableData->getRawHeader();
        $nome = $this->tableData->getInfoField("Estacao","");
        $lat = $this->tableData->getInfoField("Latitude","");
        $lng = $this->tableData->getInfoField("Longitude","");
        
        return  sprintf("INSERT INTO Estacao (criador, id, tipo, nome, cidade, lat, lng, header) VALUES ('%s', %d, %d, '%s', '%s', %f, %f, '%s');", $criador, $id, $tipo, $nome, $cidade, $lat, $lng, $header);
    }
    
    
    public function insertColumns($table_id) {
        $columnsAndTypes = $this->tableData->getTypes();
        $columns = array_keys($columnsAndTypes);
        $types = array_values($columnsAndTypes);
        $query = [];
        
        $typesFilter = array(
			"BaseCell" => 0,
			"BaseCellDecimal" => 1,
			"BaseCellDate" => 2,
			"BaseCellTime" => 3,
			"BaseCellDateTime" => 4,
        );
        
        for ($i = 0; $i < count($columnsAndTypes); $i++) {
            $query[] = sprintf("(%d, %d, '%s', %d)", $table_id, $i, $columns[$i], $typesFilter[$types[$i]]);
        }
        
        return "INSERT INTO Coluna (tabela, id, nome, tipo) VALUES ". implode(",",$query).";";
    }
    
    public function insertRows($table_id, $criador) {
        $rows = $this->tableData->getRows();
        $rowQuery = [];
        
        foreach($rows as $lin => $row)
        {
			$rowQuery[] = sprintf("(%d, %d, '%s')", $table_id, $lin, $criador);
        }

        return "INSERT INTO LinhaDono VALUES ".implode(",",$rowQuery).";";	
    }
    
    public function insertCells($table_id) {
        $rows = $this->tableData->getRows();
        $cellQuery = [];
        
        foreach($rows as $lin => $row)
        {
			foreach($row as $col => $cell)
			{
				$cellQuery[] = sprintf("(%d, %d, %d, '%s')", $table_id, $lin, $col, $cell->getRawInfo());
			}
        }

        return "INSERT INTO Celula(tabela,linha,coluna,raw) VALUES ".implode(",",$cellQuery).";";	
    }
    
    public function insertRawRows($table_id,$criador,$rows,$offset=0)
    {
		$rowQuery = [];
        
        foreach($rows as $lin => $row)
        {
			$rowQuery[] = sprintf("(%d, %d, '%s')", $table_id, $lin+$offset, $criador);
        }

        return "INSERT INTO LinhaDono VALUES ".implode(",",$rowQuery).";";	
	}
	
    public function insertRawCells($table_id,$rows,$offset=0)
    {
		$cellQuery = [];
		
		foreach($rows as $lin => $row)
		{
			foreach($row as $col => $cell)
			{
				$cellQuery[] = sprintf("(%d, %d, %d, '%s')", $table_id, $lin+$offset, $col, $cell->getRawInfo());
			}
		}
		
		return "INSERT INTO Celula(tabela,linha,coluna,raw) VALUES ".implode(",",$cellQuery).";";	
	}

    public function buildLoginQuery($email) {
        return sprintf("SELECT * FROM Usuario WHERE email='%s';",
            $email);
    }
    
    public function getStations($fields = ["*"]) {
        return sprintf("SELECT %s FROM Estacao;", implode(",", $fields));
    }

    public function getStation($nome) {
        return sprintf("SELECT * FROM Estacao WHERE nome=%s;", $nome);
    }
    
    public function getColumns($station_id)
    {
		return sprintf("SELECT * FROM Coluna WHERE tabela=%d;", $station_id);
	}
	
	public function locateRowInStation($station_id,$row)
	{
		$sqlRow = [];
		
		foreach($row as $key => $value)
		{
			$sqlRow[] = sprintf("(coluna='%s' AND raw='%s')",$key,$value);
		}
		
		return sprintf("
			SELECT
				linha
			FROM
			(
				SELECT
					linha,
					COUNT(*) as c
				FROM 
					Celula
				WHERE
					tabela=%d
					AND
					(%s)
				GROUP BY
					linha
			) AS T
			WHERE c=%d;", $station_id, implode(" OR ",$sqlRow), sizeof($row));
	}
	
	public function getRowCountInStation($station_id)
	{
		return sprintf("SELECT Count(*) FROM Celula WHERE tabela=%d AND coluna=0;", $station_id);
	}
 
    public function insertUser($nome, $email, $senha, $admin) {
        return sprintf("INSERT INTO Usuario (nome, email, senha, is_admin) VALUES ('%s', '%s', '%s', %d);", $nome, $email, $senha, $admin);
    }
    
    public function updatePass($email, $senha){
        return sprintf("UPDATE Usuario SET senha='%s' WHERE email='%s';", $senha, $email);
    }
    
    public function getPermissions() {
        return sprintf("SELECT * FROM Permissao;");
    }
    
    public function getUserPermission($email) {
        return sprintf("SELECT permissao FROM UsuarioPermissao WHERE usuario='%s';", $email);
    }
    
    public function getAdminEmail() {
        return sprintf("SELECT email FROM Usuario WHERE is_admin=1;");
    }
    public function insertUserPermission($email, $id) {
       return sprintf("INSERT INTO UsuarioPermissao (usuario, permissao) VALUES ('%s', %d);", $email, $id);
    }
	
	public function getColumns($fields = ["*"])
	{
		return sprintf("SELECT %s FROM Coluna;",implode(",",$fields));
	}
	
	public function getRows($fields = ["*"])
	{
		return sprintf("SELECT %s FROM LinhaDono;",implode(",",$fields));
	}
	public function getCells($fields = ["*"])
	{
		return sprintf("SELECT %s FROM Celula;",implode(",",$fields));
	}
	
	public function updateAdmin($email){
		return sprintf("UPDATE Usuario SET is_admin=1 WHERE email='%s';", $email);
	} 
}

?>
