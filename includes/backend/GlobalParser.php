<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GlobalParser
 *
 * @author Luciano
 */
 
include_once "ParserInterface.php";
include_once "HidroParser.php";
include_once "ClimateParser.php";

class GlobalParser implements ParserInterface {
    public function parse($content)
	{
		try
		{
			return (new HidroParser())->parse($content);
		}
		catch(ContentNotParseable $e)
		{
			try
			{
				return (new ClimateParser())->parse($content);
			}
			catch(ContentNotParseable $e2)
			{
				throw new ContentNotParseable();
			}
		}
	}
}

/*$p = new GlobalParser();
$file = fopen("../../local/EHM02_31102013.txt","r");
$contents = fread($file,filesize("../../local/EHM02_31102013.txt"));
$contents = mb_convert_encoding($contents,"UTF-8","ISO-8859-1");
fclose($file);

var_dump($p->parse($contents));*/
