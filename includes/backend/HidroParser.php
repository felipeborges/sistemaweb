<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author Luciano
 */

include_once "third/Unidecode.php";
include_once "BaseCell.php";
include_once "TableData.php";
include_once "ContentNotParseable.php";

class HidroParser implements ParserInterface
{
    public function parse($content) {
        $table = new TableData();
        $lines = new ArrayObject();
        $columns = new ArrayObject();
        $classes = new ArrayObject();
        $i = 0;
        $j = 0;
        
        
        $content = trim($content);
        $lines_vec = explode("\n", $content);
        
        $raw_header = "";
        $top_header = TRUE;
        $columns->append("Time Stamp");
        $max = sizeof($lines_vec);
        
        
        for ($i = 0; $i < $max; $i++) {
            if (preg_match("/---+/",$lines_vec[$i]))
            {
                $i++;
                break;
            }
            
            $raw_header = $raw_header . $lines_vec[$i] . "\n";
            
            if ($i == 0) {
                continue;
            }
            if ($top_header) {
                if (\str_word_count($lines_vec[$i],0)==0) {
                    $top_header = false;
                    continue;
                }

                $last_field = null;
                
                $splitted = explode(",",$lines_vec[$i]);
                foreach ($splitted as $info) {
                    $temp = explode(": ", $info, 2);
                    if (sizeof($temp) == 1) {
                        $temp = explode(" = ", $info, 2);
                    }
                    if (sizeof($temp) == 1) {
                        if ($last_field == null) {
                            throw new ContentNotParseable();
                        } else {
                            
                            $table->setInfoField($last_field, $table->getInfoField($last_field, "") . ", " . trim($temp[0]));
                        }
                    } else {
                        if (sizeof($temp) != 2) {
                            throw new ContentNotParseable();
                        }

                        $last_field = Unidecode::decode(trim($temp[0]));
                        $table->setInfoField($last_field,trim($temp[1]));
                    }
                }
            } else {
                if (str_word_count($lines_vec[$i],0) == 0)
                    continue;

                $temp = explode(":", $lines_vec[$i], 2);

                if (sizeof($temp) != 2) {
                    throw new ContentNotParseable();
                }

                $temp[0] = Unidecode::decode(trim($temp[0]));
                if (strpos($temp[0],'sensores') !== false ){
                    continue;
                }

                $columns->append($temp[0]);
            }
        }
        
        for ($j = 0; $j < 2; $j++)
        {
            for (; $i < sizeof($lines_vec); $i++) {
                if (preg_match("/-+/",$lines_vec[$i])) {
                    $i++;
                    break;
                }
            }
        }
        $sample_data = preg_split("/\s\s+/", trim($lines_vec[$i]));
        
        if (sizeof($sample_data) != sizeof($columns)) {
            throw new ContentNotParseable();
        }

        for ($j = 0; $j < sizeof($sample_data); $j++) {
            $class = "BaseCell";

            if (preg_match("/^\d+([\.,]\d+)?$/",$sample_data[$j])) {
                $class = "BaseCellDecimal";
            } else if (preg_match("/^\d{2}\/\d{2}\/\d{4}\s\d{2}:\d{2}:\d{2}$/",$sample_data[$j])) {
                $class = "BaseCellDateTime";
            }
            $classes->append($class);

            $column_name = $columns[$j];

            try {
                $table->addColumn($column_name, $class);
            } catch (ColumnAlreadyExists $e) {
                for ($k = 2;; $k++) {
                    $neo_column_name = $column_name + " " + $k;

                    try {
                        $table->addColumn($neo_column_name, $class);

                        break;
                    } catch (ColumnAlreadyExists $e2) {
                        
                    }
                }
            }
        }

        for (; $i < sizeof($lines_vec); $i++) {
            $row = new ArrayObject();
            $row_data = preg_split("/\s\s+/",trim($lines_vec[$i]));
            
            if (sizeof($row_data) != sizeof($columns)) {
                throw new ContentNotParseable();
            }

            for ($j = 0; $j < sizeof($row_data); $j++) {
                $o = $row_data[$j];

                if ($classes[$j] == "BaseCellDecimal") {
                    $o = new BaseCellDecimal(str_replace(",", ".", $o));
                } else if ($classes[$j] == "BaseCellDateTime") {
                    $temp = preg_split("/\s/", $o);
                    $tempDate = explode("/", $temp[0]);
                    $tempTime = explode(":", $temp[1]);
                    
                    $o = BaseCellDateTime::createFromSpecific(
						$tempDate[2],
						$tempDate[1],
						$tempDate[0],
						$tempTime[0],
						$tempTime[1],
						$tempTime[2]
                    );
                }
                else
                {
					$o = new BaseCell($o);
				}

                $row->append($o);
            }

            try {
                $table->addRow($row);
            } catch (TypeMismatch $e) {
				echo $e;
                throw new ContentNotParseable();
            } catch (RowLengthMismatch $e) {
                throw new ContentNotParseable();
            }
        }

        return $table;
    }
}
/*
$p = new HidroParser();
$file = fopen("../../local/EH_HS_01_31102013_1603_COTA-092.txt","r");
$contents = fread($file,filesize("../../local/EH_HS_01_31102013_1603_COTA-092.txt"));
$contents = mb_convert_encoding($contents,"UTF-8","ISO-8859-1");
fclose($file);

var_dump($p->parse($contents))
*/
?>
