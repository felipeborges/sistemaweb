<?php

while(!file_exists("config.php"))
{
	chdir("..");
}

include_once "includes/backend/GlobalParser.php";
include_once "includes/backend/QueryBuilder.php";
include_once "config.php";

class Loader {
    private $parser;
    private $db;
    private $queryBuilder;
    
    public function __construct() {
        $this->parser = new GlobalParser();
        $this->db = new mysqli(constant('DB_HOST'), constant('DB_USER'), constant('DB_PASSWORD'), constant('DB_NAME'));
    }

    public function openFile($path) {
        $contents = NULL;

        try {
            $file = fopen($path, "r");
            $contents = fread($file, filesize($path));
            $contents = mb_convert_encoding($contents, "UTF-8", "ISO-8859-1");
            fclose($file);    
        } catch (Exception $e) {
            echo $e->getMEssage();
        }
        
        return $contents;    
    }
    
    public function parse($path) {
        $tableData = NULL;
        try {
            $tableData = $this->parser->parse($this->openFile($path));
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $tableData;
    }
    
    public function addStation($tableData,$creator)
    {
        $this->queryBuilder = new QueryBuilder($tableData);
        
        $this->db->query($this->queryBuilder->insertStation($creator));
        
        $station_id = $this->db->insert_id;
        
        $this->db->query($this->queryBuilder->insertTable($station_id));
        
        $this->db->query($this->queryBuilder->insertColumns($station_id));
        
        $this->db->query($this->queryBuilder->insertRows($station_id,$creator));
        
        $this->db->query($this->queryBuilder->insertCells($station_id));
        
        echo $this->db->error;
    }
    
    public function updateStation($tableData,$user,$station_id)
    {
		$this->queryBuilder = new QueryBuilder();
		
		$result = $this->db->query($this->queryBuilder->getColumns($station_id));
		
		$i = 0;
		
		$typesFilter = array(
			"BaseCell" => "0",
			"BaseCellDecimal" => "1",
			"BaseCellDate" => "2",
			"BaseCellTime" => "3",
			"BaseCellDateTime" => "4",
        );
		
		while($row = mysqli_fetch_array($result))
		{
			if(($i >= sizeof($tableData->getColumns())) || ($row["nome"] != $tableData->getColumns()[$i]) || ($row["tipo"] != $typesFilter[$tableData->getTypes()[$tableData->getColumns()[$i]]]))
			{
				return false;
			}
			
			$i++;
		}
		
		$row = $tableData->getRow(0);
		$nrow = [];
		
		foreach($row as $key => $value)
		{
			$nrow[$key] = $value->getRawInfo();
		}
		
		$result = $this->db->query($this->queryBuilder->getRowCountInStation($station_id));
		$totalInDB = mysqli_fetch_array($result)[0];
		
		$result = $this->db->query($this->queryBuilder->locateRowInStation($station_id,$nrow));
		
		if(!$result->num_rows)
		{
			$rowsToAdd = $tableData->getRows(0);
		}
		else
		{
			$firstRowAlreadyOnDB = mysqli_fetch_array($result)["linha"];
			
			$rowsToAdd = $tableData->getRows($totalInDB-$firstRowAlreadyOnDB);
		}
		
		if(sizeof($rowsToAdd))
		{
			$this->db->query($this->queryBuilder->insertRawCells($station_id,$rowsToAdd,$totalInDB));
			$this->db->query($this->queryBuilder->insertRawRows($station_id,$user,$rowsToAdd,$totalInDB));
		}
		
		return true;
	}
}

$l = new Loader();
$table = $l->parse("/var/www/localphp/sistemaweb-parser-port/local/teste2.txt");
$l->updateStation($table,"",3);


?>
