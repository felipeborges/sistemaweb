<?php

class BaseCell
{
	private $rawInfo;
	
	public function __construct($rawInfo)
	{
		$this->rawInfo = $rawInfo;
	}
	
	public function getRawInfo()
	{
		return $this->rawInfo;
	}
	
	public function __toString()
	{
		return $this->rawInfo;
	}
	
	public function compareTo($that)
	{
		if($this->rawInfo == $that->rawInfo)
		{
			return 0;
		}
		
		return ($this->rawInfo < $that->rawInfo) ? -1 : 1;
	}
}

?>
