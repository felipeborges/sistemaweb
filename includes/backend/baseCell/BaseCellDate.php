<?php

class BaseCellDate extends BaseCell
{
	private $year;
	private $month;
	private $day;
	
	public function __construct($rawInfo)
	{
		parent::__construct($rawInfo);
		
		$tempDate = explode("-", $rawInfo);
		
		$this->year = $tempDate[0];
		$this->month = $tempDate[1];
		$this->day = $tempDate[2];
	}
	
	public static function createFromSpecific($year, $month, $day)
	{
		return new BaseCellDate($year."-".$month."-".$day);
	}
	
	public function compareTo($that)
	{
		if($this->year == $that->year)
		{
			if($this->month == $that->month)
			{
				if($this->day == $that->day)
				{
					return 0;
				}
				
				return ($this->day < $that->day) ? -1 : 1;
			}
			
			return ($this->month < $that->month) ? -1 : 1;
		}
		
		return ($this->year < $that->year) ? -1 : 1;
	}
}

?>
