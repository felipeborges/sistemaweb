<?php

class BaseCellDateTime extends BaseCell
{
	private $d;
	
	public function __construct($rawInfo)
	{
		parent::__construct($rawInfo);
		
		$this->d = new DateTime($rawInfo,new DateTimeZone("UTC"));
	}
	
	public static function createFromSpecific($year, $month, $day, $hour, $minute, $second)
	{
		return new BaseCellDateTime($year."-".$month."-".$day." ".$hour.":".$minute.":".$second);
	}
	
	public function compareTo($that)
	{
		if($this->d == $that->d)
		{
			return 0;
		}
		
		return ($this->d < $that->d) ? -1 : 1;
	}
}

?>
