<?php
ob_start();
session_start();

include_once('config.php');
include_once('backend/QueryBuilder.php');

class LoginSession {
	private $db;
	private $queryBuilder;

	function __construct() {
		$this->db = new mysqli(constant('DB_HOST'), constant('DB_USER'), constant('DB_PASSWORD'), constant('DB_NAME'));
		$this->queryBuilder = new QueryBuilder(null);
	}

	public function login($email, $password) {
		$query = $this->queryBuilder->buildLoginQuery($email);
		
		$result = $this->db->query($query);
		if ($this->userNotFound($result)) {
			throw new Exception("User not found");
		} else {
			$userData = mysqli_fetch_array($result);
			// check whether password is valid
			if ($this->authUser($password, $userData)) {
				// create login session 
				session_regenerate_id();
				$_SESSION['session_user_email'] = $userData['email'];
				session_write_close();
			} else {
				throw new Exception("Invalid password");
			}
		}
	}

	public function hashPassword($password) {
		return hash(PSW_HASH_FUNC, $password);
	}

	private function userNotFound($result) {
		return ($result->num_rows == 0);
	}

	private function authUser($password, $userData) {
		return (hash(PSW_HASH_FUNC, $password) == $userData['senha']);
	}

	public function logout() {
		unset($_SESSION['session_user_email']);
	}

	public function is_authenticated() {
		return isset($_SESSION['session_user_email']);
	}
	
	public function getUserData()
	{
		$query = $this->queryBuilder->buildLoginQuery($_SESSION['session_user_email']);
		
		$result = $this->db->query($query);
		
		return mysqli_fetch_array($result);
	}

	public function userIsAdmin() {
		$userData = $this->getUserData();

		return $userData['is_admin'];
	}
}

?>
