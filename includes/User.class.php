<?php

require('LoginSession.class.php');

class User {
    private $email;
    private $password;
    private $permissions;
    private $loginSession;

    function __construct($email) {
        $this->email = $email;

        $this->loginSession = new LoginSession();
    }

    public function login($password) {
        try {
            $this->loginSession->login($this->email, $password);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function logout() {
        $this->loginSession->logout();
    }

    public function is_authenticated() {
        return $this->loginSession->is_authenticated();
    }

    public function getEmail() {
        return $this->email;
    }
    
    public function getUser()
    {
		return $this->loginSession->getUserData();
	}

    public function isAdmin() {
        return $this->loginSession->userIsAdmin();
    }
}

class AnonymousUser extends User {
    function __construct() {

    }

    public function is_authenticated() {
        return false;
    }
}
