<?php

/*
 * MySQL settings
 * 
 * Please, do not commit changes to this file unless you're defining a
 * new constant.
 * 
 */

if(!file_exists("local__config.php"))
{
	copy("default__config.php","local__config.php");
}

include_once "local__config.php";

$constants = array(
	'DB_NAME' => 'testedb2',
	'DB_USER' => 'root',
	'DB_PASSWORD' => 'gremio',
	'DB_HOST' => 'localhost',
	'PSW_HASH_FUNC' => 'sha256'
);

foreach($constants as $name => $value)
{
	if(!defined($name))
	{
		define($name,$value);
	}
}

?>
