<?php

require_once('includes/User.class.php');

$msg = "";

if (isset($_POST['email']) && isset($_POST['password']))
{
	$msg = "123";
	
    try
    {
        $user = new User($_POST['email']);
        $user->login($_POST['password']);

        if($user->is_authenticated())
        {
            $msg = "0";
		}
		else
        {
            $msg = "1";
        }
    }
    catch (Exception $e)
    {
		$msg = "1";
    }

}

header('Location: /imanust/login_form/?msg='.$msg);

?>
