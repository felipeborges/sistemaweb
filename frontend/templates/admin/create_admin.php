<?php

include_once "includes/SendEmail.class.php";

global $_CACHE;
global $_DATA;
global $_MYSQL;

//$_CACHE->get_cached_and_die_or_start("Home",3600);

$renderer = new Rendered("frontend/templates/admin/base.php");

ob_start();

$css = ob_get_contents();
ob_end_clean();

ob_start();

?>

<?php
	function hashPassword($password) {
		return hash(PSW_HASH_FUNC, $password);
	}
?>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<div style="text-align:center;">
	<h1>Criação de um novo Administrador para o sistema</h1><br>
	<p>
		<center><h3>Informações do novo admin:</h3></center><br>
		<form action="" method="post">
			Nome: <input type="text" name="name"><br>
			E-mail: <input type="text" name="email"><br>
			Senha: <input type="text" name="pass"><br>
			<input type="submit" name="send" value="Submeter">
		</form>
		
		<?php
			if (isset($_POST["send"])) {
				$name=$_POST["name"];
				$email=$_POST["email"];
				
				$obj= new SendEmail($email);
				
				$val=$obj->verify_email();
				if ($val){
					
					$queryBuilder = new QueryBuilder(null);
					$query1 = $queryBuilder->buildLoginQuery($email);
					$result=$_MYSQL->query($query1);
					if ($row = mysqli_fetch_array($result)){
						//email já cadastrado, tornar usuario admin
						$query2 = $queryBuilder->updateAdmin($email);
						$result=$_MYSQL->query($query2);
						if ($result)
							echo "<br>Administrador criado com sucesso.";
						else
							echo "<br>Desculpe, ocorreu um erro.";
						
						
					}
					else{
						//cadastrar email e tornar admin
						
						//gerar senha caso não seja informada
						if (empty($_POST["pass"])){
							$pass=$email.time();
							$pass = substr(hashPassword($pass),0,6);
							echo "<br>A senha do novo administrador é: ".$pass."<br>";
						}
						else{
							$pass = $_POST["pass"];
						}
						
						//cadastrar usuario no BD
						$query2 = $queryBuilder->insertUser($name, $email, hash('sha256', $pass), 1);
						$_MYSQL->query($query2);
						
						if ($result)
							echo "<br>Administrador criado com sucesso.";
						else
							echo "<br>Desculpe, ocorreu um erro.";
						
						
					}
				}
				else{
					echo "<br>Desculpe, formato de e-mail inválido. Insira-o novamente.";
				}
			}
			
		?>
		
	</p>

<div>

<?php

$body = ob_get_contents();
ob_end_clean();

echo $renderer->render([
	"title" => "Home",
	"body" => $body,
	"css" => $css,
]);

//$_CACHE->finish();

?>
