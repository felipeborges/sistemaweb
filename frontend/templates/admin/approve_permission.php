<?php

include_once "includes/SendEmail.class.php";
include_once "config.php";

global $_CACHE;
global $_DATA;
global $_MYSQL;


$renderer = new Rendered("frontend/templates/admin/base.php");

ob_start();

$css = ob_get_contents();
ob_end_clean();

ob_start();

?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

<div class="home" style="text-align:center;">
	<h1>Aprovar permissões solicitadas por email</h1>
	<br>
	<p> 
		<h2>Permissões existentes:</h2>
		<?php
			$queryBuilder = new QueryBuilder(null);
			$query1 = $queryBuilder->getPermissions();
			$result=$_MYSQL->query($query1);
			
			while($row = mysqli_fetch_array($result)) { ?>
			
				<LI><?=$row["nome"]?> ID=(<?=$row["id"]?>)</LI>
			
			<?php 
				$last_perm=$row["id"];
			} ?>
			<br>
			
			<div>
				<form action="" method="post">
				<br><h2>Caro administrador, informe o endereço de email do usuário e qual a permissão que será dada à ele, informando seu respectivo ID.</h2><br>
				Email: <input type="text" name="email"><br>
				Permissão ID: <input type="text" name="id"><br>
				Comentário para o usuário: <textarea name="comment" rows="5" cols="50"></textarea><br>
				<input type="submit" name="send" value="Aprovar">
				<input type="submit" name="send2" value="Rejeitar">
				</form> 
 
				<?php
				if (isset($_POST["send"])) {
					$email=$_POST["email"];
					$id=$_POST["id"];
					
					$obj= new SendEmail($email);
		
					$val=$obj->verify_email();
					if ($val){
						$query2 = $queryBuilder->buildLoginQuery($email);
						$result=$_MYSQL->query($query1);
						
						if ($row = mysqli_fetch_array($result)){
							if ($id<1 or $id>$last_perm){
								echo "<br>Desculpe, permissão não existente.";
							}
							else{
								if (empty($_POST["comment"]))
									{$comment = "-";}
								else{
									$comment = $_POST["comment"];
								}
								
								$query3 = $queryBuilder->insertUserPermission($email, $id);
								$result=$_MYSQL->query($query3);
								
								if ($result){
									$obj->send_email_to_user("Aprovada", $comment);
									echo "<br>Foi enviado um e-mail ao usuário informando que a solicitação foi aprovada. Obrigado.";
								}
								else
									echo "<br>Desculpe, ocorreu um erro";
							}
							
						}
						else{
							echo "<br>Desculpe, esse e-mail não está cadastrado.";
						}
						
					}
					else{
						echo "<br>Desculpe, formato de e-mail inválido. Insira-o novamente.";
					}
				}
				
				if (isset($_POST["send2"])) {
					$email=$_POST["email"];

					$obj= new SendEmail($email);
		
					$val=$obj->verify_email();
					if ($val){
						$query2 = $queryBuilder->buildLoginQuery($email);
						$result=$_MYSQL->query($query1);
						
						if ($row = mysqli_fetch_array($result)){
							if (empty($_POST["comment"]))
									{$comment = "-";}
							else{
								$comment = $_POST["comment"];
							}
							
							$obj->send_email_to_user("Rejeitada", $comment);
								
							echo "Foi enviado um e-mail ao usuário informando que a solicitação foi rejeitada. Obrigado.";
							
						}
						else{
							echo "Desculpe, esse e-mail não está cadastrado.";
						}
					}
					else{
						echo "Desculpe, formato de e-mail inválido. Insira-o novamente.";
					}
				}
				
				?>
				
			</div>
		     
	</p>
</div>

<?php

$body = ob_get_contents();
ob_clean();

echo $renderer->render([
        "title" => "Request Access",
        "body" => $body,
]);

//$_CACHE->finish();

?>
