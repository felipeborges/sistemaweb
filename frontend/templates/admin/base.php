<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="/static/css/reset.css" type="text/css"/>
		<link rel="stylesheet" href="/static/css/admin_panel.css" type="text/css"/>
		
		<link rel="stylesheet" href="/static/fonts/earthbound/font-face.css" type="text/css"/>
		%css%
		<title>Painel de Administração - %title%</title>
	</head>
	<body>
		<div id="container">
			<div class="header">
				<h1 class="logo"><a href="/imanust/home">Admin_Panel</a></h1>
				<ul class="toplinks">
					<li><a href="/imanust/admin/approve_permission">Gerenciar Permissões</a></li>
					<li><a href="#">Página</a></li>
					<li><a href="#">Página</a></li>
					<li><a href="/imanust/logout/">Logout</a></li>
				</ul>
			</div>
			<?php

				if (isset($_SESSION['session_user_email'])) {
					$user = new User($_SESSION['session_user_email']);
					if ($user->isAdmin() == "1") {
				?>
				<div class="main_content">
					<?=(@$_GET['msg'] == "2") ? '<div class="notification_box"><p>Deslogado!</p></div>' : "" ?></p>
					%body%
				</div>
			</div>
		</div>
	</body>
</html>

<?php

}
// If user is not allowed in Admin Panel
} else { 
	header('Location: /imanust/login/');
}

$default_template_context = [
	"title" => "Base",
	"body" => "",
	"css" => "",
];

?>
