<?php

global $_CACHE;

//$_CACHE->get_cached_and_die_or_start("Home",3600);

$renderer = new Rendered("frontend/templates/admin/base.php");

ob_start();

$css = ob_get_contents();
ob_end_clean();

ob_start();

?>
<div class="home">
	Aqui vão funções especificas do Admin! 
	
	<li><a href="/imanust/create_admin">Criar novo admin</a></li>
	
</div>
<?php

$body = ob_get_contents();
ob_end_clean();

echo $renderer->render([
	"title" => "Home",
	"body" => $body,
	"css" => $css,
]);

//$_CACHE->finish();

?>
