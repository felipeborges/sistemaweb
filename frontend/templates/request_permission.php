<?php

include_once "includes/SendEmail.class.php";
include_once "config.php";

global $_CACHE;
global $_DATA;
global $_MYSQL;


$renderer = new Rendered("frontend/templates/base.php");

ob_start();

?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<div style="text-align:center;">
	<h1>Solicitação de novas permissões</h1><br>
	<br>
	<p>
		<h2>É possível solicitar as permissões listadas abaixo:</h2><br>
		<?php
			$queryBuilder = new QueryBuilder(null);
			$query1 = $queryBuilder->getPermissions();
			$result=$_MYSQL->query($query1);
			
			while($row = mysqli_fetch_array($result)) { ?>
			
				<LI><?=$row["nome"]?> ID=(<?=$row["id"]?>)</LI>
			
			<?php } ?>
			<br>
		     
		<?php
		
		if (isset($_SESSION['session_user_email'])) {
		    $user = new User($_SESSION['session_user_email']);
		?>
			<div>
				<h2>Permissões atuais do usuário:</h2><br>
				<?php
					$query2 = $queryBuilder->getUserPermission($user->getEmail());
					//$query2 = $queryBuilder->getUserPermission("user4@site.com");
					$result=$_MYSQL->query($query2);
					if ($row = mysqli_fetch_array($result)){
						echo "Permissão ID = ".$row["permissao"];
					}
					else
						echo "Usuário não possui nenhuma permissão.";
				?>
				
				<div><br>
					<form action="" method="post">
					<h2>Por favor informe qual permissão deseja solicitar, entrando com seu respectivo ID.</h2><br>
					Permissão ID: <input type="text" name="id"><br>
					<input type="submit" name="send" value="Submeter">
					</form>
				</div>
				
				<?php
					//Apenas envia um email para os admins e eles que se virem
					if (isset($_POST["send"])) {
						//
						$query3 = $queryBuilder->getAdminEmail();
						$result=$_MYSQL->query($query3);
						$id=$_POST["id"];
						
						while($row = mysqli_fetch_array($result)) {
							$end=$row["email"];
							$obj= new SendEmail($end);
							$obj->send_email_to_admin($user->getEmail(), $id);
						
						}
					
						
						echo "<br>A solicitação será processada. Poderá demorar dias, meses e até anos. Obrigado.";
					}
				?>
				
			</div>
			
		<?php } 
		else
		  echo "<br>Desculpe, mas você deve estar logado no sistema."?>
	</p>
</div>

<?php

$body = ob_get_contents();
ob_clean();

echo $renderer->render([
        "title" => "Request Access",
        "body" => $body,
]);

//$_CACHE->finish();

?>
