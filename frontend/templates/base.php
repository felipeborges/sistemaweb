<!--<? session_start(); ?>-->

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="/static/css/reset.css" type="text/css"/>
		<link rel="stylesheet" href="/static/css/base.css" type="text/css"/>
		
		<link rel="stylesheet" href="/static/fonts/earthbound/font-face.css" type="text/css"/>
		%css%
		<title>Imanust - %title%</title>
	</head>
	<body>
		<div id="container">
			<div class="header">
				<h1 class="logo"><a href="/imanust/home">Imanust</a></h1>
				<ul class="toplinks">
					<li><?=(isset($_SESSION['session_user_email'])) ? '<a href="/imanust/logout/">Logout</a>' : '<a href="/imanust/login_form/">Login</a>' ?></li>
					<li><a href="/imanust/request_access">Novo Usuario</a></li>
					<li><a href="/imanust/new_pass">Nova Senha</a></li>
					<li><a href="/imanust/request_permission">Requisitar Permissão</a></li>
					<li><a href="/imanust/about">Sobre</a></li>
					<li><a href="#">Contato</a></li>
				</ul>
			</div>
				<div class="main_content">
					<?=(@$_GET['msg'] == "2") ? '<div class="notification_box"><p>Deslogado!</p></div>' : "" ?></p>
					%body%
				</div>
			</div>
		</div>
	</body>
</html>

<?php

$default_template_context = [
	"title" => "Base",
	"body" => "",
	"css" => "",
];

?>
