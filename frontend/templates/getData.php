<?php

global $_CACHE;

//$_CACHE->get_cached_and_die_or_start("Home",3600);

ob_start();

include_once('config.php');

// Estrutura basica do grafico
$grafico = array(
    'dados' => array(
        'cols' => array(
            array('type' => 'string', 'label' => 'TimeStep'),
            array('type' => 'number', 'label' => 'Temperatura')
        ),  
        'rows' => array()
    ),
    'config' => array(
        'title' => 'Dados da estação',
        'width' => 770,
        'height' => 400
    )
);

// Consultar dados no BD
$pdo = new PDO('mysql:host='.constant('DB_HOST').';dbname='.constant('DB_NAME').'', constant('DB_USER'), constant('DB_PASSWORD'));
$sql = 'SELECT * FROM Celula Where tabela = 1 and coluna = 3';
$stmt = $pdo->query($sql);
while ($obj = $stmt->fetchObject()) {
        $grafico['dados']['rows'][] = array('c' => array(
        array('v' => 'Hora'),
        array('v' => (int)$obj->raw)
    ));
}

// Enviar dados na forma de JSON
header('Content-Type: application/json; charset=UTF-8');
echo json_encode($grafico);
exit(0);

?>

