<?php

$builder = new QueryBuilder();

global $_CACHE;

//$_CACHE->get_cached_and_die_or_start("Home",3600);

$renderer = new Rendered("frontend/templates/base.php");

ob_start();

?>

<link rel="stylesheet" href="/static/css/home.css" type="text/css"/>

<?php

$css = ob_get_contents();
ob_end_clean();

ob_start();

?>
<div class="home">
	<p>	<?php
			if (@$_GET['msg'] == "0") {
				header('Location: /imanust/');
			} else if (@$_GET['msg'] == "1") {
				echo '<div class="notification_box"><p>Email/Senha errados</p></div>';
			}
		?>
	</p>
	<div id="login_box">
		<form method="POST" action="/imanust/login/" name="login_form">
			<p><label for="email">
				Email
				<br>
				<input type="email" id="email_field" class="input" name="email" value size="20" />
				</label>
			</p>
			<p><label for="password">
				Senha
				<br>
				<input type="password" id="password_field" class="input" name="password" value size="20" />
				</label>
			</p>
			<p class="remember_password">
				<a href="/imanust/forgot_password">Esqueci minha senha</a>
			</p>
			<p class="submit">
				<input type="submit" name="submit" id="login_bttn" value="Login" />
			</p>
		</form>
	</div>
</div>
<?php

$body = ob_get_contents();
ob_end_clean();

echo $renderer->render([
	"title" => "Home",
	"body" => $body,
	"css" => $css,
]);

//$_CACHE->finish();

?>
