<?php

$builder = new QueryBuilder();

global $_CACHE;

//$_CACHE->get_cached_and_die_or_start("Home",3600);

$renderer = new Rendered("frontend/templates/base.php");

ob_start();

?>

<link rel="stylesheet" href="/static/css/home.css" type="text/css"/>

<?php

$css = ob_get_contents();
ob_end_clean();

ob_start();

?>

<div class="home">
	<h1>Estações</h1>
	<p>
		Selecione uma estação de medição abaixo para visualizar seus dados
	</p>

	<div id="station_table">
	<?php $result = $_MYSQL->query($builder->getStations(["id","nome","cidade","tipo", "lat", "lng"]));

	while($row = mysqli_fetch_array($result)) { 
		if($row["tipo"] == 0) {
				$estacao = 'climática';
		}
		if($row["tipo"] == 1) {
				$estacao = 'hidríca';
		}
		
		?>
        <div id="station_one">
		    <h1><a href="/imanust/station/<?=$row["id"]?>"><?=$row["nome"]?></a></h1>
		    <img src=<?='http://maps.googleapis.com/maps/api/staticmap?center='.$row["lat"].','.$row["lng"].'&zoom=11&size=200x200&sensor=false'?> />
		    <div class="desc">
			    <p>Estação <?=$estacao ?>, localizada em <?=$row["cidade"]?></p>
			</div>
        </div>
    <?php } ?>
	</div>
</div>

<?php

$body = ob_get_contents();
ob_end_clean();

echo $renderer->render([
	"title" => "Home",
	"body" => $body,
	"css" => $css,
]);

//$_CACHE->finish();

?>
