<?php

global $_CACHE;
global $_DATA;
$builder = new QueryBuilder();

//$_CACHE->get_cached_and_die_or_start("station_".$_DATA['station_id'],3600);

$renderer = new Rendered("frontend/templates/base.php");

ob_start();
$i = 1;
$css = ob_get_contents();
ob_end_clean();

ob_start();

?>
<link rel="stylesheet" href="/static/css/station.css" type="text/css"/>


<div class="home">
	
<?php $result = $_MYSQL->query($builder->getStations(["id","nome","cidade","lat","lng"])); 
 while($row = mysqli_fetch_array($result)) { 
	 if($_DATA['station_id'] == $row['id']) {
	 ?>
	 	
	<h1>Estação <?=$row["nome"] ?></h1>
	<p>
        <div id="station">
			    <p>Localização: <?=$row["cidade"]?></p>
			    <p>Latitude/Longitude: <?=$row["lat"]?>/<?=$row["lng"]?></p>
			   <img src="http://maps.googleapis.com/maps/api/staticmap?center=-15.800513,-47.91378&zoom=11&size=200x200&sensor=false" align="right">
			</div> 	<?php } }?>
			<br><br><br>
<a href="#" onclick="window.open('/imanust/station/graph', 'Pagina', 'STATUS=NO, TOOLBAR=NO, LOCATION=NO, DIRECTORIES=NO, RESISABLE=NO, SCROLLBARS=YES, TOP=10, LEFT=10, WIDTH=770, HEIGHT=400');">Visualizar Gráfico</a>  

	<div id="table">
	<table border=1 width=60% height=20% align=center cellpadding=1 cellspacing=1>
		<?php $result = $_MYSQL->query($builder->getColumns(["nome","id","tabela"])); ?>
		<tr>
		<?php while($col = mysqli_fetch_array($result)) { 
			
			if($col['tabela'] == $_DATA['station_id'])	{ ?>
				<td><?=$col["nome"]?></td> <?php } ?>
			
			 

  <?php } ?> </tr><tr>
		<?php $result = $_MYSQL->query($builder->getRows(["tabela"])); 
		$re = $_MYSQL->query($builder->getCells(["tabela","raw","linha"]));
		
		while($cells = mysqli_fetch_array($re)) {
			$count = count($cells);
			if ($cells["tabela"] == $_DATA['station_id']) {	
				if($cells["linha"] == $i) { ?>
					<td><?=$cells["raw"]?></td>
					
					
<?php 			}
		    }
	    } $i=$i+1?></tr>
<?php	
	for($x=0; $x<$count; $x++) {
		$re = $_MYSQL->query($builder->getCells(["tabela","raw","linha"]));
		while($cells = mysqli_fetch_array($re)) {
			if ($cells["tabela"] == $_DATA['station_id']) {
				if($cells["linha"] == $i) { ?>
					<td><?=$cells["raw"]?></td>
					
<?php 			}
		    }
	    } $i = $i+1;?></tr>
<?php } ?>
	</table>
	



</div>



<?php
$body = ob_get_contents();
ob_clean();

echo $renderer->render([
	"title" => "Visualizador de estações",
	"body" => $body,
	"css" => $css,
]);

//$_CACHE->finish();

?>

