<?php

$builder = new QueryBuilder();

global $_CACHE;

$renderer = new Rendered("frontend/templates/base.php");

ob_start();

?>

<link rel="stylesheet" href="/static/css/home.css" type="text/css"/>

<?php

$css = ob_get_contents();
ob_end_clean();

ob_start();

?>

<div class="home">
	<h1>Proposta</h1>
	<p align="justify">Temos como objetivo disponibilizar uma interface simples e intuitiva para a comunidade científica e acadêmica, permitindo acessar dados gerados por estações de medições espalhadas pelo Rio Grande do Sul e gerenciadas pela Universidade Federal de Pelotas.</p>
	<p></p>
	<h1>Equipe</h1>
	<p align="justify">O projeto é uma proposta do curso de Engenharia Hídrica em parceria com o curso de Ciência da Computação, ambos viculados ao Centro de Desenvolvimento Tecnológico da Universidade Federal de Pelotas.</p>
	<p align="justify">Programadores: Douglas Bento, Felipe Borges, Leonardo Corrêa, Luciano Luz, Muriel Franco<br> 
	Coordenadores: Responsável pelo projeto</p><br><br>
	<h1>Como contribuir?</h1>
	<p align="justify">Para contribuir para o projeto, utilize o menu principal para solicitar permissões ou entre em contato conosco através do email imanust@ufpel.edu.br</p>
	<br><br><br>
<?php

$body = ob_get_contents();
ob_end_clean();

echo $renderer->render([
	"title" => "Sobre",
	"body" => $body,
	"css" => $css,
]);

//$_CACHE->finish();

?>
