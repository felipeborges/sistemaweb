<?php

include_once "includes/SendEmail.class.php";
include_once "config.php";

global $_CACHE;
global $_DATA;
global $_MYSQL;


$renderer = new Rendered("frontend/templates/base.php");

ob_start();

?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

<div style="text-align:center;">
<h1>Cadastro no sistema</h1><br>
<form action="" method="post">
<h3>Por favor informe seu nome e endereço de e-mail.<br>Uma senha gerada automaticamente será enviada para o e-mail informado. Use-a para logar-se no sistema.</h3><br>
Nome: <input type="text" name="name"><br>
E-mail: <input type="text" name="email"><br>
<input type="submit" name="send" value="Submeter">
</form>

<br>

<?php

function hashPassword($password) {
		return hash(PSW_HASH_FUNC, $password);
}
      

if (isset($_POST["send"])) {
	$name=$_POST["name"];
	$email=$_POST["email"];
	
	//tratar email
	$obj= new SendEmail($email);
	
	$val=$obj->verify_email();
	if ($val){
		//Email válido
		
		//procurar por ocorrências no BD
		
		$queryBuilder = new QueryBuilder(null);
		$query1 = $queryBuilder->buildLoginQuery($email);
		$result=$_MYSQL->query($query1);
		
		if ($row = mysqli_fetch_array($result)){
			echo "<br>Desculpe, esse e-mail já existe.";
		}
		else{
			//email não existe
			$pass=$email.time();
			
			//mudar para o gerador de hash padrão
			//$pass = substr(md5($pass),0,6);
			$pass = substr(hashPassword($pass),0,6);
			
			//cadastrar usuario no BD
			$query2 = $queryBuilder->insertUser($name, $email, hash('sha256', $pass), 0);
			$_MYSQL->query($query2);
			
			//enviar email com a senha pro usuario
			$obj->send_email($pass);
			echo "<br>Foi enviado um e-mail informando a sua senha de login. Obrigado.";
		}
		
	}
	else{
		echo "<br>Desculpe, formato de e-mail inválido. Insira-o novamente.";
	}
	
      
	
}

?>
</div>
<?php

$body = ob_get_contents();
ob_clean();

echo $renderer->render([
        "title" => "Request Access",
        "body" => $body,
]);

//$_CACHE->finish();

?>

