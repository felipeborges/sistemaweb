<?php

include_once "includes/SendEmail.class.php";
include_once "config.php";

global $_CACHE;
global $_DATA;
global $_MYSQL;


$renderer = new Rendered("frontend/templates/base.php");

ob_start();

?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

<div style="text-align:center;">
<h1>Esqueci minha senha</h1><br>
<form action="" method="post">
<h3>Por favor informe seu endereço de e-mail.<br>Uma nova senha gerada automaticamente será enviada para o e-mail informado.</h3><br>
E-mail: <input type="text" name="email"><br>
<input type="submit" name="send" value="Submeter">
</form>

<?php

function hashPassword($password) {
		return hash(PSW_HASH_FUNC, $password);
}
      

if (isset($_POST["send"])) {
	$email=$_POST["email"];
	
	//tratar email
	$obj= new SendEmail($email);
	
	$val=$obj->verify_email();
	if ($val){
		//Email válido
		
		//verificar se está cadastrado no BD
		$queryBuilder = new QueryBuilder(null);
		$query1 = $queryBuilder->buildLoginQuery($email);
		$result=$_MYSQL->query($query1);
		
		//encontrou email
		if ($row = mysqli_fetch_array($result)){
			//email existe
			$pass=$email.time();
			$pass = substr(hashPassword($pass),0,6);
			
			$query2 = $queryBuilder->updatePass($email, $pass);
			$result=$_MYSQL->query($query2);
			//verifica sucesso na atualização da senha no BD
			if ($result){
			
				//enviar email com a nova senha pro usuario
				$obj->send_email($pass);
				echo "<br>Foi enviado um e-mail informando a sua nova senha de login. Obrigado.";
			}
			else{
				echo "<br>Desculpe, ocorreu um erro.";
			}
		}
		else{
			echo "<br>Desculpe, esse e-mail não está cadastrado.";
		}
	}

}

?>
</div>
<?php

$body = ob_get_contents();
ob_clean();

echo $renderer->render([
        "title" => "Request Access",
        "body" => $body,
]);

//$_CACHE->finish();

?>