<?php

include_once "config.php";
include_once "includes/url_dispacher.php";
include_once "includes/template_renderer.php";
include_once "includes/cache.php";
include_once "includes/backend/QueryBuilder.php";
include_once "includes/User.class.php";

global $_CACHE;
global $_MYSQL;
global $_DATA;

$_CACHE = new Cache();
$_MYSQL = new mysqli(constant('DB_HOST'), constant('DB_USER'), constant('DB_PASSWORD'), constant('DB_NAME'));

$dispacher = new URLDispacher(new URLPatterns([
		
		"/^\/$/" => "frontend/templates/home.php",
		"/^\/home\/$/" => "frontend/templates/home.php",
		"/^\/station\/(?<station_id>\d+)\/$/" => "frontend/templates/station.php",
		"/^\/station\/graph\/$/" => "frontend/templates/graph.php",
		"/^\/station\/graph\/getdata\/$/" => "frontend/templates/getData.php",
		"/^\/about\/$/" => "frontend/templates/about.php",
		
		"/^\/login\/$/" => "frontend/post/login.php",
		"/^\/logout\/$/" => "frontend/post/logout.php",
		"/^\/login_form\/$/" => "frontend/templates/login.php",
		"/^\/request_access\/$/" => "frontend/templates/request_access.php",
		"/^\/new_pass\/$/" => "frontend/templates/new_pass.php",
		"/^\/request_permission\/$/" => "frontend/templates/request_permission.php",

		"/^\/admin\/$/"  => "frontend/templates/admin/index.php",
		"/^\/create_admin\/$/"  => "frontend/templates/admin/create_admin.php",
		"/^\/admin\/approve_permission\/$/" => "frontend/templates/admin/approve_permission.php",
		
	]),"/^\/imanust/","frontend/templates/404.php");

$match = $dispacher->match($_SERVER["SCRIPT_URL"]);

if($match->found == FALSE)
{
	header("HTTP/1.0 404 Not Found");
}

if($match->include)
{
	$_DATA = $match->data;
	include $match->include;
}

?>
